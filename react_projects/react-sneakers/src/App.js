import React from 'react';
import axios from 'axios'
import {Routes,  Route} from 'react-router-dom';
import Header from "./components/Header";
import Drawer from "./components/Drawer";
import AppContext from './context';

import Home from "./pages/Home";
import Favorites from "./pages/Favorites";
import UserPage from "./pages/UserPage";
import Auth from "./pages/Auth";

function App() {
    const [items, setItems] = React.useState([]);
    const [cartItems, setCartItems] = React.useState([]);
    const [favorites, setFavorites] = React.useState([]);
    const [searchValue, setSearchValue] = React.useState('');
    const [cartOpened, setCartOpened] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(true);
    // const [avtorization, setAvtorization] =React.useState(false);
    // const [userState, setUserState] = React.useState(true);
    // const [togalAuthState, setTogalAuthState] = React.useState(false);

    React.useEffect(() => {
        async function fetchData() {
            const cartResponse = await axios.get('https://61b38024af5ff70017ca1f98.mockapi.io/cart');
            const favoritesResponse = await axios.get('https://61b38024af5ff70017ca1f98.mockapi.io/favorites');
            const itemsResponse = await axios.get('https://61b38024af5ff70017ca1f98.mockapi.io/items');

            setIsLoading(false);
            setCartItems(cartResponse.data);
            setFavorites(favoritesResponse.data);
            setItems(itemsResponse.data);
        }
        fetchData();
    }, []);


    const onAddToCart = async (obj) => {
            const findItem = cartItems.find((item) => Number(item.parentId) === Number(obj.id))
            if (findItem) {
                setCartItems((prev) => prev.filter((item) => Number(item.parentId) !== Number(obj.id)))
                await axios.delete(`https://61b38024af5ff70017ca1f98.mockapi.io/cart/${findItem.id}`)
            } else {
                setCartItems((prev) => [...prev, obj])
                const { data } = await axios.post('https://61b38024af5ff70017ca1f98.mockapi.io/cart', obj)
                setCartItems((prev) => prev.map(item => {
                    if (item.parentId === data.parentId) {
                        return {
                            ...item,
                            id: data.id
                        }
                    }
                    return item
                }))
            }
        }

    const onRemoveItem = (id) => {
        axios.delete(`https://61b38024af5ff70017ca1f98.mockapi.io/cart/${id}`);
        setCartItems((prev) => prev.filter((item)=> Number(item.id) !== Number(id)));
    };

    const onAddToFavorite = async (obj) => {
        try {
            if (favorites.find((favObj) => Number(favObj.id) === Number(obj.id))) {
                axios.delete(`https://61b38024af5ff70017ca1f98.mockapi.io/favorites/${obj.id}`);
                setFavorites((prev) => prev.filter((item) => Number(item.id) !== Number(obj.id)))
            } else {
                const {data} = await axios.post('https://61b38024af5ff70017ca1f98.mockapi.io/favorites', obj);
                setFavorites((prev) => [...prev, data]);
            }
        } catch (error) {
            alert('Не удалось добавить в избранное')
        }
    };

    const onChangeSearchInput = (event) => {
      setSearchValue(event.target.value);
    };
    
    const isItemAdded = (id) => {
        return cartItems.some((obj) =>Number(obj.parentId) === Number(id));
    };


    return (
     <AppContext.Provider value={{
         items,
         cartItems,
         favorites,
         isItemAdded,
         onAddToFavorite,
         onAddToCart,
         setCartOpened,
         setCartItems
     }}>
         <div className="wrapper">
             {cartOpened && <Drawer
                 // onClickAvt={() => setAvtorization(true)} items={cartItems}
                 onClose={() => setCartOpened(false)} onRemove={onRemoveItem}/>}
             <Header
                 // userState={userState}
                     onClickCart={() => setCartOpened(true)}  />
             {/*{avtorization ? <Auth/> : null}*/}
             <Routes>
                 <Route exact path="/" element={<Home
                     items={items}
                     cartItems={cartItems}
                     searchValue={searchValue}
                     setSearchValue={setSearchValue}
                     onChangeSearchInput={onChangeSearchInput}
                     onAddToFavorite={onAddToFavorite}
                     onAddToCart={onAddToCart}
                     isLoading={isLoading}
                 />}
                 />

                 <Route exact path="/favorites" element={<Favorites onAddToFavorite={onAddToFavorite} />} />
                 {/*{userPage
                 else outh}*/}
                 <Route exact path="/userpage" element=<UserPage  /> />
                 <Route exact path="/auth" element={<Auth
                     // togalAuthState={togalAuthState}
                     // setTogalAuthState={setTogalAuthState}
                     // setUserState={setUserState}
                 /> } />
             </Routes>


         </div>
     </AppContext.Provider>

  )
}
export default App;
