import React from 'react';
import axios from 'axios';

import Info from './Info';
import {useCart} from "./hooks/useCart";

function Drawer({onClose,onRemove, items = []}) {
    const {cartItems, setCartItems, totalPrice} = useCart();
    const [orderId, setOrderId] = React.useState(null);
    const [isOrderComplete, setIsOrderComplete] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(false);

    const onClickOrder = async () => {
        try {
            setIsLoading(true);
            const {data} = await axios.post('https://61b38024af5ff70017ca1f98.mockapi.io/orders', {
                items: cartItems,
            });
            axios.put('https://61b38024af5ff70017ca1f98.mockapi.io/cart', []);
            setOrderId(data.id);
            setIsOrderComplete(true);
            setCartItems([]);

        } catch (error) {
            alert('Не удалось сделать заказ :(');
        }
        setIsLoading(false);
    };
    return(
        <div className="overlay">
        <div className="drawer">
            <h2 className="cart-drawer">Корзина
                <img onClick={onClose} className="removeBtn" 
                     width={15} 
                     height={15} 
                     src="/img/btn-remove.svg" 
                     alt="Remove"
                />
            </h2>

                {items.length >0 ? (
                    <div>
                        <div className="items">
                            {items.map((obj) => (
                                <div key={obj.id} className="cartItem">
                                    <img width={70} height={70} src={obj.imageUrl} alt="Sneakers"
                                    />
                                    <div className="cart-remove">
                                        <p className="p-cart">{obj.title}</p>
                                        <b>{obj.price} руб.</b>
                                    </div>
                                    <img onClick={() =>onRemove(obj.id)}
                                         className="removeBtn"
                                         width={15} height={15}
                                         src="/img/btn-remove.svg"
                                         alt="Remove"/>
                                </div>
                            ))}
                        </div>
                        <div className="cartTotalBlock">
                            <ul>
                                <li className="cartTotalLi">
                                    <span>Итого:</span>
                                    <div className="cartTotalDiv"></div>
                                    <p>{totalPrice} руб.</p>
                                </li>
                                <li className="cartTotalLi">
                                    <span>Налог 5%:</span>
                                    <div className="cartTotalDiv"></div>
                                    <p>{(totalPrice) * 0.05} руб.</p>
                                </li>
                            </ul>
                            <button disabled={isLoading}  onClick={onClickOrder} className="greenButton">
                                Оформить заказ</button>
                        </div>
                    </div>
                )
                    :
                    (
                    <Info title={isOrderComplete ? "Ваш заказ оформлен!" : "Корзина пустая"}
                          description={isOrderComplete ? `Ваш заказ №${orderId}  скоро будет доставлен курьером` : "Добавьте вещь для оформления заказа" }
                          imageUrl={isOrderComplete ? "/img/done.jpg" : "/img/empty.jpg"}
                          />
                )}

        </div>
        </div>
    );
}
export default Drawer;
