import React from 'react';
import { Link } from 'react-router-dom';
import {useCart} from "./hooks/useCart";

function Header(props) {
    const {totalPrice} = useCart();
    // console.log(props.userState)
    // const isAuthLink =  () => {
    //     if  (!props.userState) {
    //         return (
    //             <Link to="/userpage" >
    //                 <img width={27} height={27} src="/img/orders.png" alt="Заказы"/>
    //                 ? userStateEmail(true) >
    //             </Link>
    //         )
    //     }else{
    //         return(
    //             <Link to="/auth">
    //                 войти
    //                 <img width={27} height={27} src="/img/user1.png" alt="Пользователь"/>
    //             </Link>
    //         )
    //
    //     }
    // }

    return(
        <header>
            <Link to="/">
            <div className="headerLeft">
                <img width={70} height={35} src="/img/logo.png" alt="Logotype"/>
                <div className="headerInfo">
                    <h3>Nike Sneakers</h3>
                    <p>Официальный магазин</p>
                </div>
            </div>
            </Link>
            <ul className="headerRight">
                <li onClick={props.onClickCart}>
                    <img width={29} height={29} src="/img/card1.png" alt="Корзина"/>
                    <span>{totalPrice} руб</span>
                </li>
                <li>
                    <Link to="/favorites">
                        <img width={29} height={29} src="/img/heart.svg" alt="Закладки"/>
                    </Link>
                </li>
                <li>
                    <Link to="/userpage" >
                        <img width={27} height={27} src="/img/orders.png" alt="Заказы"/>
                    </Link>
                </li>
                <li>
                    <Link to="/auth">
                        <div>Войти</div>
                        {/*<img width={27} height={27} src="/img/user1.png" alt="Пользователь"/>*/}
                    </Link>
                </li>
                {/*<li>*/}
                {/*    {isAuthLink()}*/}
                {/*</li>*/}



            </ul>
        </header>
    );
}
export default Header;