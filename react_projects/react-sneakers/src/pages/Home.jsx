import React from 'react';
import Card from "../components/Card";


function Home({
                  items,
                  searchValue,
                  setSearchValue,
                  onChangeSearchInput,
                  onAddToFavorite,
                  onAddToCart,
                  isLoading
              }) {
   
    const renderItems = () => {
        const filtredItems = items.filter((item) =>
                item.title.toLowerCase().includes(searchValue.toLowerCase()),
            );
        return (isLoading ? [...Array(8)] : filtredItems).map((item, index) =>(
            <Card
                key={index}
                onFavorite={(obj) => onAddToFavorite(obj)}
                onPlus={(obj) => onAddToCart(obj)}
                loading={isLoading}
                {...item}
            />

        ))

    }
    return (
        <div className="content">
            <div className="content-block">
                <h1>{searchValue ? `Поиск по запросу: "${searchValue}"` : 'Все кроссовки'}</h1>
                <div className="search-block">
                    <input className="input-search" placeholder="Поиск..."
                           onChange={onChangeSearchInput} />
                </div>
            </div>
            <div className="card-flex">
                {renderItems()}
            </div>
        </div>
    )
}
export default Home;