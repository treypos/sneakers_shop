import React from 'react';
import Card from '../components/Card';
import axios from "axios";

function UserPage() {
    const [orders, setOrders] = React.useState([])
    const [isLoading, setIsLoading] = React.useState(true)

    React.useEffect(() => {
        (async () => {
            try {
                const {data} = await axios.get('https://61b38024af5ff70017ca1f98.mockapi.io/orders');
                setOrders(data.reduce((prev, obj) => [...prev, ...obj.items], []));
                setIsLoading(false);
            } catch (error) {
                alert('Ошибка при запросе заказов');
                console.error(error);
            }
        })();
    }, []);
    return (
        <div className="content">
            <div className="content-block">
                <h1>Мои заказы</h1>
            </div>
            <div className="card-flex">
                {(isLoading ? [...Array(8)] : orders).map((item, index) => (
                    <Card
                        key={index}
                        loading={isLoading}
                        {...item}
                    />

                ))}
            </div>
        </div>
    )
}
export default UserPage;