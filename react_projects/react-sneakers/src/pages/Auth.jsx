import React, {useEffect, useState} from 'react';

const useValidation = (value, validations) => {
    const [isEmpty, setEmpty] = useState(true)
    const [minLenghtError, setMinLenghtError] = useState(false)
    const [maxLenghtError, setMaxLenghtError] = useState(false)
    const [emailError, setEmailError] = useState(false)
    const [inputValid, setInputValid] = useState(false)
    // const [togalAuthState, setTogalAuthState] = React.useState(false);

    useEffect( () => {
        for (const validation in validations) {
            switch (validation) {
                case 'minLenght':
                    value.length < validations[validation] ? setMinLenghtError(true) : setMinLenghtError(false)
                    break;
                case 'isEmpty':
                    value ? setEmpty( false) : setEmpty(true )
                    break;
                case 'maxLenght':
                    value.length > validations[validation] ? setMaxLenghtError(true) : setMaxLenghtError(false)
                    break
                case 'isEmail':
                    const re = /^(([^<>()[\],;:\s@]+(\.[^<>()[\],;:\s@]+)*)|(.+))@(([^<>()[\],;:\s@]+\.)+[^<>()[\],;:\s@]{2,})$/i;
                    re.test(String(value).toLowerCase()) ? setEmailError(false) : setEmailError(true)
                    break
            }
        }
    }, [value])

    useEffect( () => {
        if (isEmpty || maxLenghtError || minLenghtError || emailError) {
            setInputValid(false)
        } else {
            setInputValid(true)
        }
    }, [isEmpty, maxLenghtError, minLenghtError, emailError])

    return {
        isEmpty,
        minLenghtError,
        emailError,
        maxLenghtError,
        inputValid
    }
}

const useInput = (initialValue, validations) => {
    const [value, setValue] = useState(initialValue)
    const [isDirty, setDirty] = useState(false)
    const valid = useValidation(value, validations)
    const onChange = (e) => {
        setValue(e.target.value)
    }

    const onBlur = () => {
        setDirty(true)
    }

    return {
        value,
        onChange,
        onBlur,
        isDirty,
        ...valid
    }
}

const Auth = (props) => {
    const email = useInput('', {isEmpty: true, minLength: 3, isEmail: true})
    const password = useInput('', {isEmpty: true, minLength: 5, maxLength: 8})
    // const submitHandler = () => {
    //     if (props.togalAutState) {
    //         props.setTogalAuthState = (true) =
    //     }
    // }
    return (
        <div className="registration">
            <form className="avtorization">
                <h1>Авторизация</h1>
                {(email.isDirty && email.isEmpty) && <div style={{color: 'red'}}>Поле не может быть пустым</div>}
                {(email.isDirty && email.minLenghtError) && <div style={{color: 'red'}}>Некорректная длина</div>}
                {(email.isDirty && email.emailError) && <div style={{color: 'red'}}>Некорректный email</div>}
                <input onChange={e => email.onChange(e)} onBlur={e => email.onBlur(e)} value={email.value} name='email' type="text" placeholder='Введите email...'/>
                {(password.isDirty && password.isEmpty) && <div style={{color: 'red'}}>Поле не может быть пустым</div>}
                {(password.isDirty && password.maxLenghtError) && <div style={{color: 'red'}}>Он слишком длинный</div>}
                {(password.isDirty && password.minLenghtError) && <div style={{color: 'red'}}>Некорректная длина</div>}
                <input onChange={e => password.onChange(e)} onBlur={e => password.onBlur(e)} value={password.value} name='password' type="text" placeholder='Введите пароль...'/>
                <button className="avtButton" disabled={!email.inputValid || !password.inputValid} type='submit'
                >Войти</button>
            </form>
            {/*<button onClick={() => props.setTogalAuthState((prev) => !prev)}>*/}
            {/*    {*/}
            {/*        props.togalAuthState ? "Enter" : "Reg"*/}
            {/*    }*/}
            {/*</button>*/}
        </div>
    )
}

export default Auth;